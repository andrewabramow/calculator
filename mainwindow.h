#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QLineEdit>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    bool int_to_float();


public slots:
    void plus();
    void minus();
    void multiply();
    void divide();

private:
    Ui::MainWindow *ui;
    float a;
    float b;
};
#endif // MAINWINDOW_H
