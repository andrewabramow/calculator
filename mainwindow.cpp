#include "mainwindow.h"
#include "./ui_calc.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    a = 0;
    b = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::int_to_float() {
    a = ui->lineEdit->text().toFloat();
    b = ui->lineEdit_2->text().toFloat();
    return a && b ? true : false;
}

void MainWindow::plus(){
    if (int_to_float()) {
        ui->lineEdit_3->setText(QString::number(a+b));
    } else {
        ui->lineEdit_3->setText("ERROR");
    }
}
void MainWindow::minus(){
    if (int_to_float()) {
        ui->lineEdit_3->setText(QString::number(a-b));
    } else {
        ui->lineEdit_3->setText("ERROR");
    }
}
void MainWindow::multiply(){
    if (int_to_float()) {
        ui->lineEdit_3->setText(QString::number(a*b));
    } else {
        ui->lineEdit_3->setText("ERROR");
    }
}
void MainWindow::divide(){
    if (int_to_float()) {
        ui->lineEdit_3->setText(QString::number(a/b));

    } else {
        ui->lineEdit_3->setText("ERROR");
    }
}

